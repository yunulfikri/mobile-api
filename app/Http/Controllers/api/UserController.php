<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Follower;

class UserController extends Controller
{
    //
    public function myUser(Request $request)
    {
        # code... untuk menampilkan data user sendiri
        $currentUser = \Auth::user()->id;
        $myUser = UserDetail::find($currentUser);
        $myUser['name'] = \Auth::user()->name;
        $respon = [
            'status' => 'Success',
            'msg' => '',
            'data' => $myUser,
            'errors' => null,
            'status_code' => 200,  
        ];
        return response()->json($respon, 200);
    }

    public function followUser(Request $request)
    {
        # code... untuk mengikuti user tertentu
        
    }
}

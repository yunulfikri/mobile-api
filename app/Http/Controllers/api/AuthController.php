<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Log;


class AuthController extends Controller
{
    //
    public function signup(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);
        if($validator->fails()){
            $response = [
                'error' => 'Error validation',
                'message' => $validator->errors()
            ];
            return response()->json($response,500);       
        }
        
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['role'] = 'users';
        $uuid = Str::uuid();
        $user = User::create($input);
        UserDetail::create([
            'uuid' => Str::uuid(),
            'fullname' => $user->name,
            'refcode' => Str::random(6),
            'user_id' => $user->id
        ]);
        $success['token'] =  $user->createToken('MyAuthApp')->plainTextToken;
        $success['name'] =  $user->name;
        Log::create([
            'name' => 'register',
            'value' => $uuid,
            'type' => 'auth',
            'status' => 'success'
        ]);
        
        return response()->json($success, 200);
    }

    public function signin(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validate->fails()) {
            $respon = [
                'status' => 'error',
                'msg' => 'Validator error',
                'errors' => $validate->errors(),
                'content' => null,
            ];
            return response()->json($respon, 200);
        } else {
            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                $respon = [
                    'status' => 'error',
                    'msg' => 'Unathorized',
                    'errors' => null,
                    'content' => null,
                ];
                return response()->json($respon, 401);
            }

            $user = User::where('email', $request->email)->first();
            // return $user;
            if (! \Hash::check(
                $request->password, 
                $user->password, [])
            ) {
                throw new \Exception('Error in Login');
            }

            $tokenResult = $user->createToken('MyAuthApp')->plainTextToken;
            $respon = [
                'status' => 'success',
                'msg' => 'Login successfully',
                'errors' => null,
                'status_code' => 200,
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'role'=> $user->role,
                'name' => $user->name      
            ];
            return response()->json($respon, 200);
        }
    }

    public function logout(Request $request) {
        $user = $request->user();
        $user->currentAccessToken()->delete();
        $respon = [
            'status' => 'success',
            'msg' => 'Logout successfully',
            'errors' => null,
            'content' => null,
        ];
        return response()->json($respon, 200);
    }
    public function logoutall(Request $request) {
        $user = $request->user();
        $user->tokens()->delete();
        $respon = [
            'status' => 'success',
            'msg' => 'Logout successfully',
            'errors' => null,
            'content' => null,
        ];
        return response()->json($respon, 200);
    }

}

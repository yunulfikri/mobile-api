<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use Auth;
class BlogController extends Controller
{
    //

    public function store(Request $request)
    {
        # code... api for upload blog post v1

        if ($request->hasFile('image')) {
            $validator = \Validator::make($request->all(), [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:50000',
            ]);
            if($validator->fails()){
                $response = [
                    'error' => 'Error validation',
                    'message' => $validator->errors()
                ];
                return response()->json($response,500);       
            }
            $imageName = time(). '-' . \Str::random(20) . '.'.$request->image->extension();  
            $request->image->storeAs('/public', $imageName);
            $url = \Storage::url($imageName);
            $currentuserid = Auth::user()->id;
            $blog = Blog::create([
                'uuid' => \Str::uuid(),
                'title' => $request->title,
                'thumbnail' => $url,
                'content' => $request->content,
                'user_id' => $currentuserid,
                'nsfw' => $request->nsfw
            ]);
            $respon = [
                'status' => 'Success',
                'msg' => '',
                'data' => $blog,
                'errors' => null,
                'status_code' => 200,  
            ];
            return response()->json($respon, 200);
        }else{ 
            $respon = [
                'status' => 'Error',
                'msg' => 'Metode error, silahkan coba lagi',
                'errors' => "critical",
                'status_code' => 500,  
            ];
            return response()->json($respon, 500);
        }
    }

    public function update(Request $request)
    {
        # code... 
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
            'content' => 'required',
            'nsfw' => 'required',
        ]);
        if($validator->fails()){
            $response = [
                'error' => 'Error validation',
                'message' => $validator->errors()
            ];
            return response()->json($response,500);       
        }else{
            $blog = Blog::find($request->$id)->update([
                'title' => $request->title,
                'content' => $request->content,
                'nsfw' => $request->nsfw
            ]);
            $respon = [
                'status' => 'Success',
                'msg' => '',
                'data' => $blog,
                'errors' => null,
                'status_code' => 200,  
            ];
            return response()->json($respon, 200);
        }
        
    }
}
